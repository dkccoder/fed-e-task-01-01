// 使用帮助函数_average重构averageDollarValue，使用函数组合的方式实现
const fp = require('lodash/fp')
const cars = require('./02_subject')
let _average = function (xs) {
  return fp.reduce(fp.add, 0, xs) / xs.length
}
let averageDollarValue = function (cars) {
  let dollar_values = fp.map(function (car) {
    return car.dollar_value
  }, cars)
  return _average(dollar_values)
}
let f = fp.flowRight(_average, fp.map(fp.get('dollar_value')))
console.log(f(cars))