// 使用fp.flowRight()、fp.pro和fp.first()获取第一个car的name
const cars = require('./02_subject')
const fp = require('lodash/fp')
let f = fp.flowRight(fp.prop('name'), fp.first)
console.log(f(cars))