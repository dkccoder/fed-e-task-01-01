// 实现一个函数ex2，能够使用fp.first获取列表的第一个元素
const fp = require('lodash/fp')
const { Maybe, Container } = require('./03_subject')

let xs = Container.of(['do', 'ray', 'me', 'fa', 'so', 'la', 'ti', 'do'])
let ex2 = () => {
  return xs.map(x => fp.first(x))._value
}
console.log(ex2())