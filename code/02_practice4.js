// 使用flowRight写一个sanitizeNames()函数，返回一个下划线连接的小写字符串
// 把数组中的name转换为这种形式，例如：sanitizeNames(["Hello World"]) => ["hello_world"]
const fp = require('lodash/fp')
const cars = require('./02_subject')

function log (v) {
  console.log(v)
  return v
}

let _underscore = fp.replace(/\W+/g, '_')
let handleName = fp.flowRight(_underscore, fp.join(' '), fp.map(fp.lowerFirst),  fp.split(' '), fp.prop('name'))

let sanitizeNames = fp.map(handleName)

console.log(sanitizeNames(cars))