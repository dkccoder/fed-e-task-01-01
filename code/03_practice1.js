// 使用fp.add(x, y)和fp.map(f, x)创建一个能让functor里的值增加的函数ex1
const fp = require('lodash/fp')
const { Maybe, Container } = require('./03_subject')

let maybe = Maybe.of([5, 6, 1])
let ex1 = (y) => {
  return maybe.map(x => fp.map(fp.add(y))(x))
}
// maybe.map(x => x + 1)
console.log(ex1(2))