// 使用Maybe重写ex4，不要有if语句
const fp = require('lodash/fp')
const { Maybe, Container } = require('./03_subject')

let ex4 = function (n) {
  if (n) {
    return parseInt(n)
  }
}

let myEX4 = (n) => {
  return Maybe.of(n).map(x => parseInt(x))._value
}
console.log(myEX4(10.2))
console.log(myEX4(null))