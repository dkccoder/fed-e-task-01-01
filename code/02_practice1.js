// 使用函数组合fp.flowRight()重新实现下面这个函数
const cars = require('./02_subject')
const fp = require('lodash/fp')

let isLastInStock = function(cars) {
  // 获取最后一条数据
  let last_car = fp.last(cars)
  // 获取最后一条数据的in_stock属性值
  return fp.prop('in_stock', last_car)
}

let f = fp.flowRight(fp.prop('in_stock'), fp.last)
console.log(f(cars))
