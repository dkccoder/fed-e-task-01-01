// 将下面异步代码使用Promise的方法改进
// setTimeout(function () {
//   var a = 'hello'
//   setTimeout(function () {
//     var b = 'lagou'
//     setTimeout(function () {
//       var c = 'I ❤️ U'
//       console.log(a + b + c)
//     }, 10);
//   }, 10);
// }, 10)
Promise.resolve('hello')
  .then(value => value + 'lagou')
  .then(value => {
    console.log(value + 'I ❤️ U')
  })