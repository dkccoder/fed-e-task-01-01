// 实现一个函数ex3，使用safeProp和fp.first找到user的名字的首字母
const fp = require('lodash/fp')
const { Maybe, Container } = require('./03_subject')

let safeProp = fp.curry(function (x, o) {
  return Maybe.of(o[x])
})
let user = { id: 2, name: 'Albert' }
let ex3 = () => {
  return safeProp('name', user).map(x => fp.first(x))._value
}
console.log(ex3())