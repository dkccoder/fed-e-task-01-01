// Promise的回调是微任务，排在宏任务之前，绝大多数回调都是宏任务
// MutationObserver、process.nextTick都是微任务

const { resolve } = require("path");
const { reject } = require("lodash");

const promise = new Promise(function(resolve, reject){
  resolve(100)
  // reject(new Error('promise rejected'))
})

promise.then(function(value) {
  console.log('resolve', value)
}, function(error) {
  console.log('reject', error)
})



