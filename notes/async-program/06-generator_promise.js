// function * main () {
//   const users = yield ajax('/api/test.json')
//   console.log(users)

//   const posts = yield ajax('/api/posts.json')
//   console.log(posts)
// }

// const g  = main()


// const result = g.next()
// // result = { value: ajax('/api/test.json'), done: false}
// result.value.then(data => {
//   // g.next(data)
//   const result2 = g.next(data)

//   result2.value.then(data2 => {
//     g.next(data2)
//     // 还可以继续执行generator里后面的函数，直到返回的done为true
//   })
// })

// // 生成器函数执行器
// function handleResult (result) {
//   if (result.done) return
//   result.value.then(data => {
//     handleResult(g.next(data))
//   }, error => {
//     g.throw(error)
//   })
// }

// async返回的是一个promise对象
Promise.resolve(1)
  .then(2)
  .then(Promise.resolve(3))
  .then(console.log)

