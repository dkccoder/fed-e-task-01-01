const MyPromise = require('./07_mypromise')

let promise = new MyPromise((resolve, reject) => {
  resolve('成功')
  reject('失败')
})
promise.then(value => {
  console.log(value)
}, reason => {
  console.log(reason)
})