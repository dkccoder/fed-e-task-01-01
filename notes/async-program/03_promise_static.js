// Promise静态方法
// promise.resolve()可以快速返回一个状态为fulfilled的对象
Promise.resolve('foo')
  .then(function (value) {
    console.log(value)
  })