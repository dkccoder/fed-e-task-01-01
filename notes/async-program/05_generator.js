// 生成器函数
// 定义的函数在调用的时候并不会立刻执行，而是会得到一个生成器对象
// 通过next()方法才会执行函数体,next方法可以传入参数，这个参数会作为yield的返回值，可以在生成器内部接收到
// 通过throw方法可以向生成器内部抛出一个异常，内部往下执行就会得到这个异常

function * foo () {
  console.log('start')

  // yield可以向外返回一个对象，并暂停该函数的运行，下一次调next就会从暂停的地方继续运行
  // 对象的value就是yield的值，done表示该生成器是否运行完成
  const res1 = yield '暂停一次'

  console.log(res1)

  const res2 = yield '暂停两次'

  console.log(res2)
  try {
    const res3 = yield '接收异常'
    console.log(res3)
  } catch (e) {
    console.log(e)
  }
}

const generator = foo()
// 执行到第一个yield就暂停，下一次从yield左边或者下面开始执行
console.log(generator.next('1'))
console.log(generator.next('2'))
console.log(generator.next('3'))
generator.throw(new Error('Generator error'))