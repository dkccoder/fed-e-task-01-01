// Promise.all方法会同步执行里面所有的promise，可以同步执行多个promise
// Promise.race方法在其中一个promise执行结束后就会立即返回结果

var promiseAll = Promise.all([
  new Promise(1),
  new Promise(2)
  //···
])

promiseAll.then(function(values) {
  // 这里的values就是一个数组，包含了所有的promise返回的结果
}).catch(function(error) {
  // 其中一个promise失败了都会进入catch
})


// allurl是一个包含了多个url地址的json，请求回来之后再请求里面的每个地址
ajax('allurl')
  .then(value => {
    const urls = Object.values(value)
    const tasks = urls.map(url => ajax(url))
    return Promise.all(tasks)
  })
  .then(values => {
    console.log(values)
  })


const request = ajax('/api/posts.json')
const timeout = new Promise((resolve, reject) => {
  setTimeout(() => reject(new Error('timeout')), 500)
})
Promise.race([
  request,
  timeout
])
// 5毫秒中接口请求成功了就会进入then
.then(value => {
  console.log(value)
})
// 如果5毫秒接口没有返回就会进入catch
.catch(error => {
  console.log(error)
})