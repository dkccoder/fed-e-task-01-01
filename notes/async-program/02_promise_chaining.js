// Promise对象的then方法会返回一个全新的Promise对象
// 后面的then方法就是在为上一个then返回的Promise注册回调
// 前面then方法中回调函数的返回值会作为后面then方法回调的参数
// 如果回调中返回的是Promise，那后面的then方法的回调会等待它的结束

function ajax(url) {
  return new Promise(function(resolve, reject) {
    var xhr = new XMLHttpRequest()
    xhr.responseType = 'json'
    xhr.onload = function() {
      if (this.status === 200) {
        resolve(this.response)
      } else {
        reject(new Error(this.statusText))
      }
    }
    xhr.send()
  })
}

// 普通调用，then方法的第二个参数是给当前的promise对象指定的失败回调
ajax('')
  .then(function onFulfilled(value) {
    console.log('onfulfilled', value)
  }, function onRejected(error) {
    console.log('onrejected', error)
  })

// 链式调用，catch里面的回调是经过第一个then之后返回的新的promise对象，只是把错误传递了下去
ajax('')
  .then(function onFulfilled(value) {
    console.log('onfulfilled', value)
  })
  .catch(function onRejected(error) {
    console.log('onrejected', error)
  })