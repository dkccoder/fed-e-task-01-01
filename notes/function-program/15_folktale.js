// folktale是一个标准的函数式编程库，只提供了一些函数处理的操作，例如compose、curry

const { compose, curry } = require('folktale/core/lambda')
const { toUpper, first } = require('lodash/fp')

// folktale中的curry第一个参数是传入的形参个数，为了避免错误
let f = curry(2, (x, y) => {
  return x + y
})
console.log(f(1)(2))


let fn = compose(toUpper, first)
console.log(fn(['one', 'two']))