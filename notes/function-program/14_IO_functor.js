// IO函子中的_value是一个函数，即把函数作为值来处理
// 它可以把不纯的动作存储到_value中惰性执行，使当前操作纯，把不纯的操作交给调用者来处理

const fp = require('lodash/fp')

class IO {
  static of(value) {
    return new IO(function() {
      return value
    })
  }

  constructor(fn) {
    this._value = fn
  }

  map(fn) {
    // 把当前的value和传入的fn组合成一个新的函数
    return new IO(fp.flowRight(fn, this._value))
  }
}


let r = IO.of(process).map(p => p.execPath)
console.log(r);
console.log(r._value())
