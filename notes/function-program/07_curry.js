// 带有硬编码的纯函数
// function checkAge(age) {
//   let min = 18
//   return age >= min
// }

// 普通的纯函数
// function checkAge(min, age) {
//   return age >= min
// }
// // min为18时重复计算
// console.log(checkAge(18, 20))
// console.log(checkAge(18, 22))

// 优化后的纯函数，即函数的柯里化：把接受多个参数的函数变成接受单一的参数的函数，并且返回接受余下参数的新函数
// function checkAge(min) {
//   return function(age) {
//     return age >= min
//   }
// }
// 箭头函数柯里化
let checkAge = min => (age => age >= min)
let checkAge18 = checkAge(18)
let checkAge20 = checkAge(20)
console.log(checkAge18(20))
console.log(checkAge18(24))