// 记忆函数:  可以缓存计算的结果
const _ = require('lodash')

function getArea(r) {
  console.log(r)
  return Math.PI * r *r
}

// let getAreaWithMemory = _.memoize(getArea)
// console.log(getAreaWithMemory(4))
// console.log(getAreaWithMemory(5))
// console.log(getAreaWithMemory(4))

// 模拟momize
// momize参数为一个函数，并且要返回一个函数, 通过内部对象来缓存计算的结果，
// 由于是纯函数所以可以把输入的参数作为键，计算结果作为值（固定的输出可以得到固定的结果）
function memoize(f) {
  let cache = {}
  return function () {
    // arguments是伪数组不能作为键
    let key = JSON.stringify(arguments)
    // 有缓存的话直接返回结果，没有的话就进行计算
    // arguments的值不清楚，可以用apply方法展开传递
    cache[key] = cache[key] || f.apply(f, arguments)
    return cache[key]
  }
}

let getAreaWithMemory = memoize(getArea)
console.log(getAreaWithMemory(4))
console.log(getAreaWithMemory(5))
console.log(getAreaWithMemory(4))