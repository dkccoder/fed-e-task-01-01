// 纯函数：相同的输入始终会得到相同的输出
// 副作用：所有的外部交互都可能对纯函数带来副作用使之变得不纯，比如配置文件、数据库、用户输入等，使得通用性下降不适合扩展和可重用性
// slice纯函数  splice不纯的函数

let array = [1, 2, 3, 4, 5]

console.log(array.slice(0,3))
console.log(array.slice(0,3))
console.log(array.slice(0,3))

console.log(array.splice(0,3))
console.log(array.splice(0,3))
console.log(array.splice(0,3))

// 纯函数
function getSum(n1, n2) {
  return n1 + n2
}
console.log(getSum(1, 2))
console.log(getSum(1, 2))
console.log(getSum(1, 2))