// apply可以把arguments伪数组展开作为参数传递
function once(fn) {
  let done = false
  return function () {
    if (!done) {
      done = true
      return fn.apply(this, arguments)
    }
  }
}

let pay = once(function(money) {
  console.log(`消费了${money}元`)
})

pay(5)
pay(6)
pay(7)