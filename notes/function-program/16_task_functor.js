// Task处理异步任务
const fs = require('fs')
const { task } = require('folktale/concurrency/task')

// 读取文件的函数
function readFile(filename) {
  // task库里面的固定参数resolver对象,提供了resolve方法和reject方法
  return task(resolver => {
    // fs模块的readFle函数用来读取文件，第一个参数为文件路径，第二个为文件编码，第三个是一个回调函数
    fs.readFile(filename, 'utf-8', (err, data) => {
      // 出错reject
      if (err) resolver.reject(err)
      // 成功resolve
      resolver.resolve(data)
    })
  })
}

readFile('package.json')
  .run() //执行task函子
  .listen({
    onRejected: err => {
      console.log(err)
    },
    onResolved: value => {
      console.log(value)
    }
  }) //监听当前的执行状态