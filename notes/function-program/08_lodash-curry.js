// lodash柯里化的参数顺序就是接受顺序
const _ = require('lodash')

function getSum(a, b, c) {
  console.log(a, b, c)
  return a + b + c
}

const curried = _.curry(getSum)
// console.log(curried(1))
console.log(curried(1)(2, 3))
console.log(curried(1)(2)(3))

// 模拟curry函数