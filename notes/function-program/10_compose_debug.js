// 函数组合调试
// NEVER SAY DIE --> never-say-die
const _ =  require('lodash')
// fp模块已经经过柯里化处理，即函数优先，数据滞后
const fp = require('lodash/fp')


// 调试函数,将接受到上一个函数的返回值打印出来并传递给下一个函数
const log = v => {
  console.log(v)
  return v
}
const trace = _.curry((tag, v) => {
  console.log(tag, v)
  return v
})

// 函数组合都只能接受一个参数，所以要先柯里化之后再组合，如果使用fp模块就不需要这样处理
const split = _.curry((sep, str) => _.split(str, sep))
const map = _.curry((fn, array) => _.map(array, fn))
const join = _.curry((sep, array) => _.join(array, sep))

const f = _.flowRight(join('-'), trace('map之后'), map(_.toLower), log, split(' '))
console.log(f('NEVER SAY DIE'))
// FP模块
const fn = fp.flowRight(fp.join('-'), fp.map(fp.toLower), fp.split(' '))
console.log(fn('NEVER SAY DIE'))

// 注：lodash中的map遍历的函数接受的参数有三个，第一个是当前处理的元素，第二个是索引，第三个是数组
console.log(_.map(['23', '8', '10'], parseInt))
// fp的map遍历的函数只接受当前处理的元素
console.log(fp.map(parseInt, ['23', '8', '10']))