// Either函子类似于if else方法，可以用来做异常处理
class Left {
  static of (value) {
    return new Left(value)
  }

  constructor(value) {
    this._value = value
  }

  map(fn) {
    return this
  }
}

class Right {
  static of (value) {
    return new Right(value)
  }

  constructor(value) {
    this._value = value
  }

  map(fn) {
    return Right.of(fn(this._value))
  }
}

// let r1 = Right.of(12).map(x => x + 2)
// let r2 = Left.of(12).map(x => x + 2)
// console.log(r1)
// console.log(r2)

// 创建一个方法来把字符串转换成json对象
function parseJson(str) {
  // 该方法可能会出错，所以使用try catch
  try {
    return Right.of(JSON.parse(str))
  } catch (e) {
    return Left.of({error: e.message})
  }
}

let r = parseJson("{ name: zs }")   
          .map(x => x.name.toUpperCase())    
console.log(r)
let p = parseJson('{ "name": "zs" }')
          .map(x => x.name.toUpperCase())
console.log(p)