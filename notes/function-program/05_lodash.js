const _ = require('lodash')

const array = ['jack', 'tom', 'lucy', 'kate']

console.log(_.first(array))
console.log(_.last(array))

console.log(_.toUpper(array))
console.log(_.toUpper(_.first(array)))

console.log(_.reverse(array))
console.log(_.reverse(array))

const result = _.each(array, (item, index) => {
  console.log(item, index)
})
console.log(result)