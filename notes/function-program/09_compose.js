// 函数组合
// function compose(f, g) {
//   return function(value) {
//     return f(g(value))
//   }
// }

// function reverse(array) {
//   return array.reverse();
// }

// function first(array) {
//   return array[0]
// }

// let last = compose(first, reverse)

// console.log(last([4,3,2,1]))

// lodash函数组合
const _ = require('lodash')

const reverse = arr => arr.reverse()
const first = arr => arr[0]
const toUpper = s => s.toUpperCase()

const f = _.flowRight(toUpper, first, reverse)

console.log(f(['one', 'two', 'three']))

// 模拟flowRight
// compose的参数为若干个函数
// function compose(...args) {
//   return function (value) {
//     return args.reverse().reduce(function (acc, fn) {
//       return fn(acc)
//     }, value)
//   }
// }
const compose = (...args) => value => args.reverse().reduce((acc, fn) => fn(acc), value)
const fn = compose(toUpper, first, reverse)
console.log(fn(['one', 'two', 'three']))