// 函子Functor：一个特殊的容器，容器里面包含了一个值和一个对该值进行处理和变形的方法
// of方法生成一个新的函子对象，还有一个map方法来返回新的函子
// 函数式编程的运算不直接操作值， 而是由函子完成

class Container {
  static of(value) {
    return new Container(value)
  }

  constructor(value) {
    this._value = value
  }

  map(fn) {
    return Container.of(fn(this._value))
  }
}

let r = Container.of(5).map(x => x+1)
console.log(r)