function forEach(array, fn) {
  for (let i = 0; i < array.length; i++) {
    fn(array[i])
  }
}

var arr = [1, 2, 3, 4, 5];
// var brr = [];
// forEach(arr, function(item) {
//   brr.push(item + 1);
// });
// console.log(brr);

function filter(array, fn) {
  let results = [];
  for(let i = 0; i < array.length; i++) {
    if (fn(array[i])) {
      results.push(array[i]);
    }
  }
  return results;
}

// var crr = filter(arr, function(item) {
//   return item % 2 === 0;
// })
// console.log(crr);

function map(array, fn) {
  let results = [];
  for (let i =0; i < array.length; i++) {
      results.push(fn(array[i]));
  }
  return results;
}

var drr = map(arr, function(item) {
  return item % 2 ===0;
});
console.log(drr);