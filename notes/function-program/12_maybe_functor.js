// MayBe函子可以对外部传入空值的情况做处理

class MayBe {
  static of(value) {
    return new MayBe(value)
  }

  constructor(value) {
    this._value = value
  }

  map(fn) {
    return this.isNothing() ? MayBe.of(null) : MayBe.of(fn(this._value))
  }

  isNothing() {
    return this._value === null || this._value === undefined
  }
}

let r = MayBe.of(null)
          .map(x => x.toUpperCase())
console.log(r)

// 多次调用就不知道在何时传入了异常值，需要either函子
let p = MayBe.of('hello world')
          .map(x => x.toUpperCase())
          .map(x => null)
          .map(x => x.split(' '))
console.log(p)